# Travel Buddy (Back-end)

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

Front-end: https://gitlab.com/GSaunier/nwt-front.git

## Installation

```bash
NodeJs :
$ npm install

MongoDb (sans docker) port: 27017
apt-get install -y mongodb-org
```
## Mise en place de l'application

```bash
- `git clone https://gitlab.com/GSaunier/nwt-back.git`

- `npm install -g npm@latest`

- `npm install -g yarn`

- `yarn global add @nestjs/cli`

- Se rendre dans le dossier racine du projet front-end

- `yarn install`
```

## Running the app



```bash
Initialization with script mongoDB and fetch ressources :

npm run start:build

```

## Présentation

  .   https://docs.google.com/presentation/d/1Kw_iqH-Net3pVFUHyl9SCadW-KDBf3-92BsMGRrT848/edit?usp=sharing
    
   Ou pdf de la présentation disponible dans le dossier "resources/Travel Buddy.pdf"

