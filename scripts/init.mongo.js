/**
 * This script is to insert initial data inside the collection users of the database nwt
 * You can use it with mongo-shell or a tool like Robo3T
 */

// Insert users array

// all password = pass
db.dropDatabase();

db.getCollection('users').insertMany([
    {
        "_id" : ObjectId("5dcf2f5a8f3e091d8fe121c2"),
        "pseudo" : "fujine",
        "password" : "$2b$16$c.AZPE7gjHq9oJvxs9e/quwlKR9oEp60nNRkJ4.gfdnI5ZG.VVdw2",
        "firstname" : "Guillaume",
        "lastname" : "Saunier",
        "email": "guillaume.saunier@outlook.fr",
        "role" : "user"
    },
    {
        "_id" : ObjectId("5dcf2f8f8f3e091d8fe121c3"),
        "pseudo" : "test",
        "password" : "$2b$16$AyNBIGjiCMU/IrPziuLQZe5u2o8RIhBs9VkS5vq/koyJ056aNEd6.",
        "firstname" : "Guillaume",
        "lastname" : "Saunier",
        "email": "fujine16@gmail.com",
        "role" : "user"
    },
    {
        "_id" : ObjectId("5dcf2fa48f3e091d8fe121c4"),
        "pseudo" : "admin",
        "password" : "$2b$16$8zNgSJQ0u2GmSnTciXJQVuL4nCeGqcPyRB/EYpfFiqxF/RiCwNBWu",
        "firstname" : "Admin",
        "lastname" : "Admin",
        "email": "admiNN@yahoo.fr",
        "role" : "admin"
    },
    {
        "_id" : ObjectId("5dcf2fb58f3e091d8fe121c5"),
        "pseudo" : "visiteur",
        "password" : "$2b$16$qgEHHLprzJARIJAgkq6mWe85nZNvuElyJQI0gjes8jHewIABRrdjq",
        "firstname" : "Jean",
        "lastname" : "Pass",
        "email": "oups@outlook.fr",
        "role" : "user"
    }
]);

db.getCollection('destinations').insertMany([
    {
        "_id": ObjectId("5763cd4dc378a38ecd38d537"),
        "country": "France",
        "description": "Great place to be",
        "image": {
            "type": "image/jpg",
            "filename": "place-vosges-img_0028-c2a9-jf-hamard.jpg"
        },
        "region": "Vosges",
        "city": "Epinal",
        "contacts": [
            {
                "_id" : ObjectId("5dcf2fb58f3e091d8fe121c5"),
                "firstname" : "Jean",
                "lastname" : "Pass",
                "email": "oups@outlook.fr",
            }
        ],
        "travelers": []
    },
    {
        "_id": ObjectId("5763cd4dc378a38e9d38d537"),
        "country": "Spain",
        "description": "The most beautiful city of Spain",
        "image": {
            "type": "image/jpeg",
            "filename": "barcelona.jpg"
        },
        "region": "Catalonia",
        "city": "Barcelone",
        "travelers": [
            {
                "_id" : ObjectId("5dcf2fb58f3e091d8fe121c5"),
                "firstname" : "Jean",
                "lastname" : "Pass",
                "email": "oups@outlook.fr",
            }
        ],
        "contacts": []
    },
    {
        "_id": ObjectId("5763cd4dc78fa38e9d38d537"),
        "country": "France",
        "description": "Somewhere in France",
        "region": "Vosges",
        "city": "Darnieulles",
        "travelers": [
            {
                "_id" : ObjectId("5dcf2fb58f3e091d8fe121c5"),
                "firstname" : "Jean",
                "lastname" : "Pass",
                "email": "oups@outlook.fr",
            }
        ],
        "contacts": [
            {
                "_id" : ObjectId("5dcf2f5a8f3e091d8fe121c2"),
                "firstname" : "Guillaume",
                "lastname" : "Saunier",
                "email": "guillaume.saunier@outlook.fr",
            }
        ]
    },
    {
        "_id": ObjectId("57648fbdc378a38e9d38d537"),
        "country": "Belgium",
        "description": "Very good french fries",
        "region": "Wallonie",
        "city": "Liège",
        "travelers": [],
        "contacts": []

    }
]);

// display the final initial data
db.getCollection('users').find({});
