#!/bin/bash
BLUE='\033[0;34m'
GREEN='\033[1;32m'
DONE='\033[1;36m'
NC='\033[0m' # No Color
NC='\033[0m' # No Color
echo "${BLUE}[Delete]${NC} ${GREEN}Delete exesting 'assets' folder${NC}"
rm -r assets
echo "${BLUE}[Delete]${NC} ${DONE}[DONE]${NC}"
echo "${BLUE}[Create File]${NC} ${GREEN}Create the different image folders of Destination base entity${NC}"
mkdir -p "assets/destinations/5763cd4dc378a38e9d38d537"
mkdir -p "assets/destinations/5763cd4dc378a38ecd38d537"
echo "${BLUE}[Create File]${NC} ${DONE}[DONE]${NC}"
echo "${BLUE}[Copy File]${NC} ${GREEN}Copy the different images of Destination base entity${NC}"
cp -r "scripts/resources/barcelona.jpg" "assets/destinations/5763cd4dc378a38e9d38d537"
cp -r "scripts/resources/place-vosges-img_0028-c2a9-jf-hamard.jpg" "assets/destinations/5763cd4dc378a38ecd38d537"
echo "${BLUE}[Copy File]${NC} ${DONE}[DONE]${NC}"
echo "${BLUE}[BDD]${NC} ${GREEN}MongoDB initialization with script${NC}"
mongo localhost:27017/Buddy scripts/init.mongo.js
echo "${BLUE}[BDD]${NC} ${DONE}[DONE]${NC}"
sleep 2
nest $1




