export const USERS = [
  {
    id: '5763cd4d9d2a4f259b53c901',
    pseudo: 'test',
    password: '$2b$16$QrYGFTpRBwAZzmrGF8ij3.xyUqZwxq6PXvqgm2KV.7I4rUaKBR2ku', //pass
    firstname: 'theo',
    lastname: 'boby',
    role: 'admin'
  },
  {
    id: '5763cd4d51fdb6588742f99e',
    pseudo: 'ici',
    password: '$2b$16$TgLi.6Uqb.0erXzzv.0TeeFhLn0ByPg.Ps.DjZdsh1I3.BA6TcTie', //motpass
    firstname: 'Guillaume',
    lastname: 'esstla',
    role: 'user'
  },
  {
    id: '5763cd4dba6362a3f92c954e',
    pseudo: 'dernier',
    password: '$2b$16$cbGIo/WNfUSG6XQyz94YsuGUnd.tz3x/ZTogbZ9Roq9Eq5mZEjB.W',//pasword
    firstname: 'Noah',
    lastname: 'Julien',
    role: 'user'
  },
];
