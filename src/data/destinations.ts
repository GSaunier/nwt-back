export const DEST = [
  {
    id: '5763cd4d9d2a4f259b53c901',
    country: 'France',
    region: 'Lorraine',
    city: 'Nancy',
    contacts: [{
      id: '5dcf2f5a8f3e091d8fe121c2',
      firstname: 'Guillaume',
      lastname: 'Saunier',
      email: "guillaume.saunier@outlook.fr",
    }],
    travelers: []
  },
  {
    id: '5763cd4d9d2a48859b53c901',
    country: 'Espagne',
    region: 'Catalogne',
    city: 'Barcelone',
    contacts: [{
      id: '5dcf2fb58f3e091d8fe121c5',
      firstname: 'Jean',
      lastname: 'Pass',
      email: "oups@outlook.fr",
    }],
    travelers: [
      {
        id: '5dcf2f5a8f3e091d8fe121c2',
        firstname: 'Guillaume',
        lastname: 'Saunier',
        email: "guillaume.saunier@outlook.fr",
      }
    ]
  },
  {
    id: '5763cd4d9d2a48549b53c901',
    country: 'France',
    region: 'Picardie',
    city: "Leblanc",
    contacts: [],
    travelers: [{
      id: '5dcf2fb58f3e091d8fe121c5',
      firstname: 'Jean',
      lastname: 'Pass',
      email: "oups@outlook.fr",
    }],
  },
  {
    id: '5763cd4d9d2745259b53c901',
    country: 'Belgique',
    region: 'Wallonie',
    city: 'Liège',
    contacts: [],
    travelers: []


  },
];
