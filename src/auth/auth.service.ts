import { ConflictException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { UserEntity } from '../users/entities/user.entity';
import { Observable, of, throwError } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDto } from '../users/dto/login-user.dto';
import * as bcrypt from 'bcrypt'

@Injectable()
export class AuthService {
  constructor(private readonly _usersService: UsersService, private readonly _jwtService: JwtService) {}

  validateUser(pseudo: string): Observable<UserEntity> {
    return this._usersService.findOneByPseudo(pseudo).pipe(
      flatMap(_ => !!_ ?
        of(_) :
        undefined
      ),
    );
  }

  validateLogin(loginUserDto: LoginUserDto): Observable<UserEntity> {
    return this._usersService.findOneByPseudo(loginUserDto.pseudo).pipe(
      flatMap(_ => bcrypt.compareSync(loginUserDto.password,_.password) ?
        of(_) :
        throwError(new ConflictException("Bad credetials"))
      ),
    );
  }


  login(loginUserDto: LoginUserDto): Observable<UserEntity> {
    return this.validateLogin(loginUserDto).pipe(
      flatMap(_ => this._usersService.updateToken(_.id, this._jwtService.sign({ username: _.pseudo, sub: _.pseudo }))),
    );
  }

}
