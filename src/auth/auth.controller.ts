import { Body, ClassSerializerInterceptor, Controller, Get, Post, Request, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { LoginUserDto } from '../users/dto/login-user.dto';
import { Observable } from 'rxjs';
import {
  ApiBadRequestResponse, ApiBearerAuth,
  ApiConflictResponse, ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiUnauthorizedResponse, ApiUnprocessableEntityResponse,
  ApiUseTags,
} from '@nestjs/swagger';
import { UserEntity } from '../users/entities/user.entity';
import { RegisterUserDto } from '../users/dto/register-user.dto';
import { UsersService } from '../users/users.service';

@ApiUseTags('auth')
@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
  constructor(private readonly _authService: AuthService, private readonly _usersService: UsersService) {}

  @ApiCreatedResponse( {description: 'Authentification succed', type: UserEntity})
  @ApiNotFoundResponse( { description: 'User doesn\'t found'})
  @ApiBadRequestResponse( {description: `Bad Request`, type: LoginUserDto})
  @ApiConflictResponse({description: 'Bad credentials'})
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @Post('login')
  login(@Body() loginUserDto: LoginUserDto): Observable<UserEntity> {
    return this._authService.login(loginUserDto);
  }

  @ApiBearerAuth()
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiOkResponse({description: `Get authentify user`, type: UserEntity})
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get('profile')
  @UseGuards(AuthGuard('jwt'))
  getProfile(@Request() req): Observable<UserEntity> {
    return req.user;
  }

  /**
   * Handler to answer POST /users route
   *
   * @param registerUserDto data to create
   *
   * @returns Observable<UserEntity>
   */
  @ApiBadRequestResponse({description: `Bad request`, type: RegisterUserDto})
  @ApiCreatedResponse({description: `Get the nes User`, type: UserEntity})
  @ApiConflictResponse( {description: 'Pseudo  already exists'})
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @Post('/register')
  register(@Body() registerUserDto: RegisterUserDto): Observable<UserEntity> {
    return this._usersService.register(registerUserDto);
  }
}
