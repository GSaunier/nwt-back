
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { jwtConstants } from './constants';
import { AuthService } from './auth.service';
import { flatMap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly _autService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  validate(paylod) {
    return this._autService.validateUser(paylod.sub).pipe(
      flatMap(
        _ => !!_ ?
          of(_) :
          throwError(new UnauthorizedException(`Doesn't have permission to access`))
      )
    )
  }
}
