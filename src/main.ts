import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { Logger, ValidationPipe } from '@nestjs/common';
import { SwaggerConfig } from './interfaces/swagger-config.interface';
import { AppConfig } from './interfaces/app-config.interface';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as Config from 'config';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { DestinationsModule } from './destinations/destinations.module';

async function bootstrap(config: AppConfig, swaggerConfig: SwaggerConfig) {
  // create NestJS application
  const fAdapt = new FastifyAdapter({logger: true});
  fAdapt.register(require('fastify-multipart'),{
    limits: {
      fieldSize: 1000000000, // Max field value size in bytes
      fileSize: 100000000000,      // For multipart forms, the max file size
    }
  });

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    fAdapt
  );

  app.enableCors();
  // use global pipe validation
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  // create swagger options
  const options = new DocumentBuilder()
    .setTitle(swaggerConfig.title)
    .setDescription(swaggerConfig.description)
    .setVersion(swaggerConfig.version)
    .addTag('users')
    .addTag('auth')
    .addTag('destinations')
    .addBearerAuth()
    .build();

  // create swagger document
  const usersDocument = SwaggerModule.createDocument(app, options, {
    include: [UsersModule,AuthModule,DestinationsModule],
  });

  // setup swagger module
  SwaggerModule.setup(swaggerConfig.path, app, usersDocument);

  // launch server
  await app.listen(config.port, config.host);
  Logger.log(`Application served at http://${config.host}:${config.port}`, 'bootstrap');
  Logger.log(`Application documentation at http://${config.host}:${config.port}/documentation`, 'bootstrap');
}

bootstrap(Config.get<AppConfig>('server'), Config.get<SwaggerConfig>('swagger'));
