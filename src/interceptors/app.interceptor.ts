import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor, NotAcceptableException } from '@nestjs/common';
import { merge, Observable, of, throwError } from 'rxjs';
import { catchError, filter, flatMap, map, tap } from 'rxjs/operators';
import { ServerResponse } from 'http';
import { FastifyReply } from 'fastify';

@Injectable()
export class AppInterceptor implements NestInterceptor {
  /**
   * Class constructor
   */
  constructor() {
  }

  /**
   * Intercepts all HTTP requests and responses
   *
   * @param context
   * @param next
   */
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const cls = context.getClass();
    const handler = context.getHandler();
    const response: FastifyReply<ServerResponse> = context.switchToHttp().getResponse<FastifyReply<ServerResponse>>();
    const logCtx: string = `UserInterceptor => ${cls.name}.${handler.name}`;

      return next.handle()
        .pipe(
          catchError(e => { Logger.log(e); return  throwError(e) }),
          map(_ => of(_)),
          flatMap((obs: Observable<any>) =>
            merge(
              obs
                .pipe(
                  filter(_ => !!_),
                  map(_ => _),
                ),
              obs
                .pipe(
                  filter(_ => !_),
                  tap(_ => response.status(204)),
                  map(_ => _),
                ),
            )))


        /*tap(
          _ => Logger.log(!!_ ? _ : 'NO CONTENT', logCtx),
          _ => Logger.error(_.message, JSON.stringify(_), logCtx),
        ),*/

  }
}
