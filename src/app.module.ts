import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { DestinationsModule } from './destinations/destinations.module';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { DocumentsModule } from './documents/documents.module';
import * as Config from 'config';

@Module({
  imports: [
    UsersModule,
    AuthModule,
    DestinationsModule,
    MongooseModule.forRoot(Config.get<string>('mongodb.uri'),Config.get<MongooseModuleOptions>('mongodb.options')),
    DocumentsModule
  ],
})
export class AppModule {}
