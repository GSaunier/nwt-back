import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable, of, throwError } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { flatMap } from 'rxjs/operators';
import { UserEntity } from '../users/entities/user.entity';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly _reflector: Reflector) {}

  canActivate(context: ExecutionContext ): Observable<boolean> {
    let decRole = this._reflector.get<string>('role', context.getHandler());
    return context.switchToHttp().getRequest().user.pipe(
        flatMap((user:UserEntity) => (!!decRole && user.role === decRole) ?
          of(true) :
          throwError(new UnauthorizedException("Needs admin role"))
        )
    );
  }
}
