import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginUserDto {
  @ApiModelProperty({ description: 'Pseudo', example: 'test'})
  @IsString()
  @IsNotEmpty()
  pseudo: string;

  @ApiModelProperty({ description: 'Password', example: 'pass'})
  @IsString()
  @IsNotEmpty()
  password: string;
}
