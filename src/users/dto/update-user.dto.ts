import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmpty, IsNotEmpty, IsOptional, IsString, Matches, MinLength } from 'class-validator';

export class UpdateUserDto {
  @ApiModelProperty({ description: 'Pseudo', example: 'Fujine'})
  @IsString()
  @MinLength(4)
  @IsOptional()
  pseudo?: string;

  @ApiModelProperty({ description: 'Mot de passe', example: 'pass'})
  @IsString()
  @MinLength(4)
  @IsOptional()
  password?: string;

  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @IsString()
  @MinLength(2)
  @IsOptional()
  firstname?: string;

  @ApiModelProperty({ description: 'Lastname', example: 'Saunier'})
  @IsString()
  @MinLength(2)
  @IsOptional()  lastname?: string;

  @ApiModelProperty({ description: 'Email', example: 'example@test.com'})
  @IsString()
  @IsOptional()
  @Matches(/[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/)
  email?: string;

  @ApiModelProperty({ description: 'Role', example: 'user'})
  @IsString()
  @Matches(/(admin|user)/)
  @IsOptional()
  role?: string;

}
