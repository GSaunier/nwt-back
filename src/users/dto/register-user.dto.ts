import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, Matches, MinLength } from 'class-validator';

export class RegisterUserDto {
  @ApiModelProperty({ description: 'Pseudo', example: 'Fujine'})
  @IsString()
  @MinLength(4)
  pseudo: string;

  @ApiModelProperty({ description: 'Mot de passe', example: 'pass'})
  @IsString()
  @MinLength(4)
  password: string;

  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @IsString()
  @MinLength(2)
  firstname: string;

  @ApiModelProperty({ description: 'Lastname', example: 'Saunier'})
  @IsString()
  @MinLength(2)
  lastname: string;

  @ApiModelProperty({ description: 'Email', example: 'example@test.com'})
  @IsString()
  @Matches(/[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/)
  email: string;

}
