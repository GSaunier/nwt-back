import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  pseudo: {
    type: String,
    unique: true,
    required: true,
    minlength: 4,
    trim: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true,
    trim: true
  },
  firstname: {
    type: String,
    required: true,
    minlength: 4,
    trim: true,
  },
  lastname: {
    type: String,
    required: true,
    minlength: 4,
    trim: true,
  },
  role: {
    type: String,
    match: /(user|admin)/,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    match: /[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/,
    required: true,
    trim: true,
  },
  token: {
    type: String,
    required: false,
    trim: true
  },
}, {
  toJSON: { virtuals: true },
  versionKey: false,
});
