import { Body, ClassSerializerInterceptor, Controller, Delete, Get, Param, Post, Put, UseGuards, UseInterceptors } from '@nestjs/common';
import { RegisterUserDto } from './dto/register-user.dto';
import { Observable } from 'rxjs';
import { UserEntity } from './entities/user.entity';
import { UsersService } from './users.service';
import {
  ApiBadRequestResponse, ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse, ApiImplicitBody,
  ApiImplicitParam, ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse, ApiUnauthorizedResponse, ApiUnprocessableEntityResponse,
  ApiUseTags,
} from '@nestjs/swagger';
import { HandlerParams } from './validators/handler-params';
import { UpdateUserDto } from './dto/update-user.dto';
import { AppInterceptor } from '../interceptors/app.interceptor';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../roles/roles.guard';
import { Role } from '../roles/decorators/role.decorator';
import { CreateUserDto } from './dto/create-user.dto';

@ApiUseTags('users')
@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
@UseInterceptors(AppInterceptor)
export class UsersController {

  constructor(private readonly _usersService: UsersService) {}

  /**
   * Handler to answer POST /users route
   *
   * @param createUserDto data to create
   *
   * @returns Observable<UserEntity>
   */
  @ApiBearerAuth()
  @ApiBadRequestResponse({description: `Bad request`, type: RegisterUserDto})
  @ApiCreatedResponse({description: `Get the nes User`, type: UserEntity})
  @ApiConflictResponse( {description: 'Pseudo  already exists'})
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiUnauthorizedResponse({description: "Unauthorized and needs admin role"})
  @Post('')
  @Role('admin')
  @UseGuards(AuthGuard('jwt'),RolesGuard)
  create(@Body() createUserDto: CreateUserDto): Observable<UserEntity> {
    return this._usersService.create(createUserDto);
  }

  @ApiBearerAuth()
  @ApiNoContentResponse({ description: `There aren't any user` })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiOkResponse({description: `Array of users`, type: UserEntity, isArray:true})
  @ApiUnauthorizedResponse({description: "Unauthorized"})
  @Get()
  @UseGuards(AuthGuard('jwt'))
  findAll(): Observable<UserEntity[]> {
    return this._usersService.findAll();
  }

  @ApiBearerAuth()
  @ApiNotFoundResponse({description: `No user for the id`})
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiOkResponse({description: `Get user with id`, type: UserEntity})
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the user in the database', type: String })
  @ApiUnauthorizedResponse({description: "Unauthorized"})
  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  findOne(@Param() param: HandlerParams): Observable<UserEntity> {
    return this._usersService.findOne(param.id);
  }

  /**
   * Handler to answer to PUT /users/:id route
   * @param {HandlerParams} param params list of route parms to take user id
   * @param {UpdateUserDto} updateUserDto the new entries for the user
   *
   * @returns {Observable<UserEntity>} the modified user
   */
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'The User has been successfully updated', type: UserEntity })
  @ApiNotFoundResponse({ description: 'User with the given "id" doesn\'t exist in the database' })
  @ApiBadRequestResponse({ description: 'Parameter and/or payload provided are not good' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the user in the database', type: String })
  @ApiImplicitBody({ name: 'UpdateUserDto', description: 'Payload to update a user', type: UpdateUserDto })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  update(@Param() param: HandlerParams, @Body() updateUserDto: UpdateUserDto ): Observable<UserEntity> {
    return this._usersService.update(param.id,updateUserDto)
  }

  /**
   * Handler to answer to DELETE /users/:id route
   *
   * @param {HandlerParams} params list of route params to take user id
   *
   * @returns Observable<void>
   */
  @ApiBearerAuth()
  @ApiNoContentResponse({ description: 'The user has been successfully deleted' })
  @ApiNotFoundResponse({ description: 'User with the given "id" doesn\'t exist in the database' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the user in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized and needs role admin`})
  @Delete(':id')
  @Role('admin')
  @UseGuards(AuthGuard('jwt'),RolesGuard)
  delete(@Param() params: HandlerParams): Observable<void> {
    return this._usersService.delete(params.id);
  }
}
