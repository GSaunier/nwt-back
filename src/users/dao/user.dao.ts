import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../interfaces/user.interface';
import { Model, MongooseDocument } from 'mongoose';
import { from, Observable, of } from 'rxjs';
import {  flatMap, map } from 'rxjs/operators';
import { UpdateUserDto } from '../dto/update-user.dto';
import { RegisterUserDto } from '../dto/register-user.dto';
import { Destination } from '../../destinations/interfaces/destination.interface';
import * as mongoose from 'mongoose';
import { CreateUserDto } from '../dto/create-user.dto';



@Injectable()
export class UserDao {

  constructor(@InjectModel('User') private readonly _userModel: Model<User>, @InjectModel('Destination') private readonly _destinationModel: Model<Destination>) {}

  /**
   * Get all user in the database
   *
   * @return {Observable<User[]>} array of all user in the database
   */
  find(): Observable<User[]> {
    return this._toJSONArray<User>(from(this._userModel.find({})))
  }

  /**
   * Get one user with its id from the database
   *
   * @param {string} id of the user
   *
   * @return {Observable<User>} user with the id
   */
  findOneById(id: string): Observable<User> {
    return this._toJSON<User>(from(this._userModel.findById(id)))
  }

  /**
   * Check if user already exists with index and add it in users list
   *
   * @param {RegisterUserDto} registerUserDto to create
   *
   * @return {Observable<User>}
   */
  register(registerUserDto: RegisterUserDto): Observable<User> {
    return from(this._userModel.create(registerUserDto)).pipe(
      map((doc: MongooseDocument) => doc.toJSON())
    );
  }

  /**
   * Check if user already exists with index and add it in users list
   *
   * @param {RegisterUserDto} createUserDto to create
   *
   * @return {Observable<User>}
   */
  create(createUserDto: CreateUserDto): Observable<User> {
    return from(this._userModel.create(createUserDto)).pipe(
      map((doc: MongooseDocument) => doc.toJSON())
    );
  }


  /**
   * Get one user with its id from the database
   *
   * @param {string} pseudo of the user
   *
   * @return {Observable<User>} user with the pseudo
   */
  findOneByPseudo(pseudo: string): Observable<User> {
    return this._toJSON<User>(from(this._userModel.findOne({pseudo: {$regex : new RegExp(pseudo, "i") }},)));
  }

  /**
   * Modify an user with its id from the database
   *
   * @param {string} id of the user
   * @param {UpdateUserDto} updateUserDto the new user
   *
   * @return {Observable<User>} the modified user matching with id
   */
  findOneAndUpdate(id: string, updateUserDto: UpdateUserDto): Observable<User> {
    return this._toJSON<User>(from(this._userModel.findByIdAndUpdate(id,updateUserDto,{new: true}))).pipe(
      flatMap(_ => this.updateRef(_,id,'travelers') ),
      flatMap(_ => this.updateRef(_,id,'contacts')),
      flatMap(_ => this.updateRefOpinion(_,id)))
  }

  updateToken(id: string, token: string): Observable<User> {
    return this._toJSON<User>(from(this._userModel.findByIdAndUpdate(id,{token: token},{new: true})))
  }

  updateRef(user: User,id:string, type: string): Observable<User> {
    return from(this._destinationModel.updateMany({[type+"._id"]: mongoose.Types.ObjectId(id)},{$set:
        {[type+".$.firstname"]: user.firstname,
          [type+".$.lastname"]: user.lastname,
          [type+".$.email"]: user.email,
        }})).pipe(
      flatMap(_ => of(user))
    )
  }

  updateRefOpinion(user: User, id:string): Observable<User> {
    return from(this._destinationModel.updateMany({"opinions.idUser": mongoose.Types.ObjectId(id)},{$set:
        {"opinions.$.firstname": user.firstname,
          "opinions.$.lastname": user.lastname,
        }})).pipe(
      flatMap(_ => of(user))
    )
  }

  deleteRef(user: User, id: string, type: string): Observable<User> {
    return from(this._destinationModel.updateMany({},{$pull:  { [type]: {_id: id}}}
    )).pipe(
      flatMap(_ => of(user))
    )
  }

  deleteRefOpinion(user: User, id: string): Observable<User> {
    return from(this._destinationModel.updateMany({},{$pull:  { opinions: {idUser: id}}}
    )).pipe(
      flatMap(_ => of(user))
    )
  }


  /**
   * Remove an user with its id from the database
   *
   * @param {string} id of the user
   *
   * @return {Observable<User>} the removed user matching with id
   */
  findOneAndRemove(id: string): Observable<User> {
    return this._toJSON<User>(from(this._userModel.findByIdAndRemove(id))).pipe(
      flatMap(_ => this.deleteRef(_,id,"contacts")),
      flatMap(_ => this.deleteRef(_,id,"travelers")),
      flatMap(_ => this.deleteRefOpinion(_,id)))
  }

  /**
   * Transform array of document in the corresponding class
   *
   * @param {Observable<MongooseDocument[]>}obs array of MongooseDocument
   * @private
   *
   * @return {Observable<T[]>} array of class transformed
   */
  private _toJSONArray<T>(obs: Observable<MongooseDocument[]>): Observable<T[]> {
    return obs.pipe(
      map((docs: MongooseDocument[]) => (!!docs && docs.length > 0) ? docs.map(_ => _.toJSON()) : undefined)
    )
  }

  /**
   * Transform a MongooseDocument in the corresponding class
   *
   * @param {Observable<MongooseDocument>} obs of the document
   * @private
   *
   * @return {Observable<T>} class transformed
   */
  private _toJSON<T>(obs: Observable<MongooseDocument>): Observable<T> {
    return obs.pipe(
      map((doc: MongooseDocument) => !!doc ? doc.toJSON() : undefined)
    )
  }

}
