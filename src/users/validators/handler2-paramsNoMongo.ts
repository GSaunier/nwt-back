import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class Handler2ParamsNoMongo {
  @IsMongoId()
  @IsNotEmpty()
  id: string;

  @IsString()
  @IsNotEmpty()
  idProperty: string;
}
