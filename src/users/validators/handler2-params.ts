import { IsMongoId, IsNotEmpty } from 'class-validator';

export class Handler2Params {
  @IsMongoId()
  @IsNotEmpty()
  id: string;

  @IsMongoId()
  @IsNotEmpty()
  idProperty: string;
}
