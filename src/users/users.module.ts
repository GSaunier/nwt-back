import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schemas/user.schema';
import { UserDao } from './dao/user.dao';
import { DestinationSchema } from '../destinations/schemas/destination.schema';

@Module({
  imports: [ MongooseModule.forFeature([ {name: 'User', schema: UserSchema }, {name: 'Destination', schema: DestinationSchema} ])],
  controllers: [UsersController],
  providers: [UsersService, UserDao],
  exports: [UsersService, UserDao],
})
export class UsersModule {}
