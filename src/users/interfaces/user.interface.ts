import { Document } from 'mongoose';

export interface User extends Document{
  id: string;
  pseudo: string;
  password: string;
  firstname: string;
  lastname: string;
  role: string;
  email: string;
  token?: string
}
