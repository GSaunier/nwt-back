import { Exclude, Expose, Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class UserEntity {
  @ApiModelProperty({ description: 'Unique identifier in the database', example: '5763cd4dc378a38ecd387737'})
  @Expose()
  @Type(() => String)
  id: string;

  @ApiModelProperty({ description: 'Pseudo', example: 'Fujine'})
  @Expose()
  @Type(() => String)
  pseudo: string;

  @ApiModelProperty({ description: 'Password', example: 'pass'})
  @Type(() => String)
  password: string;

  @ApiModelProperty({ description: 'Firsname', example:'Guillaume'})
  @Expose()
  @Type(() => String)
  firstname: string;

  @ApiModelProperty({ description: 'Lastname', example:'Saunier'})
  @Expose()
  @Type(() => String)
  lastname: string;

  @ApiModelProperty({description: 'Role can be admin or user', example: 'user'})
  @Expose()
  @Type( () => String)
  role: string;

  @ApiModelProperty( { description: 'Email', example: "example@test.com"})
  @Expose()
  @Type(() => String)
  email: string;

  @ApiModelProperty( { description: `Token d'authentification`, example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTczNzcxMDU5LCJleHAiOjE1NzM3NzQ2NTl9.kb4xT4rGF_b7EuGfmarPJ0_SMSoqlz3MYchj-A3LEyw'})
  @Expose()
  @Type(() => String)
  token?: string;




  constructor(partial: Partial<UserEntity>) {
    Object.assign(this, partial);
  }

}
