import { ConflictException, Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { RegisterUserDto } from './dto/register-user.dto';
import { Observable, of, throwError } from 'rxjs';
import { UserEntity } from './entities/user.entity';
import { catchError, flatMap, map } from 'rxjs/operators';
import * as bcrypt from 'bcrypt'
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDao } from './dao/user.dao';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {

  private _saltRounds = 4;

  constructor(private readonly _userDao: UserDao) {
  }

  /**
   * Returns one user of the list matching pseudo in parameter
   *
   * @param {string} pseudo of the user
   *
   * @returns {Observable<UserEntity>}
   */
  findOneByPseudo(pseudo: string): Observable<UserEntity> {
    return this._userDao.findOneByPseudo(pseudo)
      .pipe(
        catchError(e => throwError(new UnprocessableEntityException(e.message))),
        flatMap(_ =>
          !!_ ?
            of(new UserEntity(_)) :
            throwError(new NotFoundException(`User with pseudo '${pseudo}' not found`)),
        ),
      );
  }

  /**
   * @return {Observable<UserEntity[]>} list of all users
   */
  findAll(): Observable<UserEntity[]> {
    return this._userDao.find().pipe(
      catchError(e => throwError(new UnprocessableEntityException(e.message))),
      map(_ => (!!_ && !!_.length) ? _.map(__ => new UserEntity(__)) : undefined),
    );
  }

  /**
   * Get one user with its id
   *
   * @param {string} id of the user
   *
   * @return {Observable<UserEntity>} user with the id
   */
  findOne(id: string): Observable<UserEntity> {
    return this._userDao.findOneById(id).pipe(
      catchError(e => throwError(new UnprocessableEntityException(e.message))),
      flatMap(_ => !!_ ?
        of(new UserEntity(_)) :
        throwError(new NotFoundException(`No user for id: ${id}`))
      ),
    );
  }

  /**
   * Update token of an user matching with id
   *
   * @param id of user
   * @param token to Authentify an user
   */
  updateToken(id:string, token: string): Observable<UserEntity> {
    return this._userDao.updateToken(id, token).pipe(
      catchError(e =>
        e.code ===11000 ?
          throwError(
            new ConflictException(e.message),
          ) :
          throwError(new UnprocessableEntityException(e.message)),
      ),
      flatMap(_ =>
        !!_ ?
          of(new UserEntity((_))) :
          throwError(new NotFoundException(`User with id '${id}' not found`)),
      ),
    )
  }

  /**
   * Check if user already exists and add it in user list
   *
   * @param {RegisterUserDto} user to create
   *
   * @return {Observable<UserEntity>}
   */
  register(user: RegisterUserDto): Observable<UserEntity> {
    return this._addUser(user)
      .pipe(
        flatMap(_ => this._userDao.register(_)),
          catchError(e => e.code === 11000 ?
            throwError(
              new ConflictException(e.message),
            ) :
              throwError(new UnprocessableEntityException())
        ),
        map(_ => new UserEntity(_))
      );
  }

  /**
   * Check if user already exists and add it in user list
   *
   * @param {CreateUserDto} user to create
   *
   * @return {Observable<UserEntity>}
   */
  create(user: CreateUserDto): Observable<UserEntity> {
    delete user["id"];
    return this._addUserAdmin(user)
      .pipe(
        flatMap(_ => this._userDao.register(_)),
        catchError(e => e.code ===11000 ?
          throwError(
            new ConflictException(e.message),
          ) :
          throwError(new UnprocessableEntityException())
        ),
        map(_ => new UserEntity(_))
      );
  }

  /**
   * Update a user in user list
   *
   * @param {string} id of the user to update
   * @param {UpdateUserDto} updateUserDto data to update
   *
   * @returns {Observable<UserEntity>} the new User
   */
  update(id: string, updateUserDto: UpdateUserDto): Observable<UserEntity> {
    !!updateUserDto.password ? updateUserDto.password = bcrypt.hashSync(updateUserDto.password,this._saltRounds) : updateUserDto;
    return this._userDao.findOneAndUpdate(id,updateUserDto).pipe(
      catchError(e =>
        e.code ===11000 ?
          throwError(
            new ConflictException(e.message),
          ) :
          throwError(new UnprocessableEntityException(e.message)),
      ),
        flatMap(_ =>
          !!_ ?
            of(new UserEntity((_))) :
            throwError(new NotFoundException(`User with id '${id}' not found`)),
        ),
      )

  }

  /**
   * Deletes one user in users list
   *
   * @param {string} id of the user to delete
   *
   * @returns {Observable<void>}
   */
  delete(id: string): Observable<void> {
    return this._userDao.findOneAndRemove(id).pipe(
      catchError(e => throwError(new NotFoundException(e.message))),
      flatMap(_ =>
        !!_ ?
          of(undefined) :
          throwError(new NotFoundException(`User with id '${id}' not found`)),
      ),
    );
  }

  /**
   * Add user with good data in users list
   *
   * @param user to add
   *
   * @returns {Observable<UserEntity>}
   *
   * @private
   */
  private _addUser(user: RegisterUserDto): Observable<RegisterUserDto> {
    return of(user)
      .pipe(
        map(_ =>
          Object.assign(_, {
            password: bcrypt.hashSync(_.password,this._saltRounds),
            role: 'user',
          })
        )
      );
  }

  /**
   * Add user with good data in users list
   *
   * @param user to add
   *
   * @returns {Observable<UserEntity>}
   *
   * @private
   */
  private _addUserAdmin(user: CreateUserDto): Observable<RegisterUserDto> {
    return of(user)
      .pipe(
        map(_ =>
          Object.assign(_, {
            password: bcrypt.hashSync(_.password,this._saltRounds),
          })
        )
      );
  }
}
