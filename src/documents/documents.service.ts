import { Injectable } from '@nestjs/common';
import * as fs from "fs";
import * as pump from 'pump'

@Injectable()
export class DocumentsService {
  /**
   * Upload the document with the path
   * @param path
   * @param filename
   * @param file
   */
  uploadFile(path: string, filename: string, file): void {
    fs.rmdirSync(path, {recursive: true});
    fs.mkdirSync(path, {recursive: true });
    pump(file, fs.createWriteStream(`${path}/${filename}`))
  }

  deleteFile(path: string) {
    fs.rmdirSync(path, {recursive: true});
  }
}
