import {
  Body,
  ClassSerializerInterceptor,
  Controller, Delete,
  Get,
  Param,
  Post, Put,
  Req,
  Res, UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse, ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiImplicitFile,
  ApiImplicitParam,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse, ApiUnauthorizedResponse, ApiUnprocessableEntityResponse, ApiUnsupportedMediaTypeResponse,
  ApiUseTags,
} from '@nestjs/swagger';

import {  Observable } from 'rxjs';
import { DestinationEntity } from './entities/destination.entity';
import { DestinationsService } from './destinations.service';
import { HandlerParams } from '../validators/handler-params';
import { DocumentsService } from '../documents/documents.service';
import { UpdateDestinationDto } from './dto/update-destination.dto';
import { UsersService } from '../users/users.service';
import { DestinationUserEntity } from './entities/destination.user.entity';
import { Handler2Params } from '../users/validators/handler2-params';
import { AppInterceptor } from '../interceptors/app.interceptor';
import { CreateDestinationDto } from './dto/create-destination.dto';
import { Role } from '../roles/decorators/role.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../roles/roles.guard';
import { DestinationOpinionEntity } from './entities/destination.opinion.entity';
import { CreateOpinionDto } from './dto/create-opinion.dto';
import { Handler2ParamsNoMongo } from '../users/validators/handler2-paramsNoMongo';


@ApiUseTags('destinations')
@ApiBearerAuth()
@Controller('destinations')
@UseInterceptors(ClassSerializerInterceptor)
export class DestinationsController {

  constructor(private readonly _destinationsService: DestinationsService, private readonly _documentsService: DocumentsService, private readonly _usersService: UsersService) {
  }


  @ApiCreatedResponse({description: "New destination created", type: DestinationEntity})
  @ApiBadRequestResponse({ description: 'Payload provided is not good' })
  @ApiConflictResponse({description: "Destination with same country, region and city already exists"})
  @ApiUnauthorizedResponse({description: `Unauthorized and needs role admin`})
  @Post()
  @Role('admin')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  create(@Body() createDestinationDto: CreateDestinationDto): Observable<DestinationEntity> {
    return this._destinationsService.create(createDestinationDto);
  }


  @ApiNoContentResponse({ description: 'No Content' })
  @ApiOkResponse({ description: 'Array of destinations', type: DestinationEntity, isArray: true })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get()
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  findAll(): Observable<DestinationEntity[]> {
    return this._destinationsService.findAll();
  }


  @ApiNotFoundResponse({ description: `Destination for the ID doesn"t exist` })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiOkResponse({ description: 'Destinations', type: DestinationEntity })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the user in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(':id')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  findOne(@Param() params: HandlerParams): Observable<DestinationEntity> {
    return this._destinationsService.findOne(params.id)
  }

  /**
   * Handler to answer to PUT /destinations/:id route
   * @param {HandlerParams} param params list of route parms to take user id
   * @param {UpdateDestinationDto} updateDestinationDto the new entries for the user
   *
   * @returns {Observable<DestinationEntity>} the modified user
   */

  @ApiOkResponse({ description: 'The Destination has been successfully updated', type: DestinationEntity })
  @ApiNotFoundResponse({ description: 'Destination with the given "id" doesn\'t exist in the database' })
  @ApiBadRequestResponse({ description: 'Parameter and/or payload provided are not good' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized and needs admin role`})
  @Put(":id")
  @Role('admin')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  update(@Param() param: HandlerParams, @Body() updateDestinationDto: UpdateDestinationDto): Observable<DestinationEntity> {
    return this._destinationsService.findOneAndUpdate(param.id, updateDestinationDto);
  }


  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the user in the database', type: String })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiNotFoundResponse({ description: 'No File or no destination found' })
  @ApiOkResponse({ description: 'The file' })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(':id/image')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  downloadImage(@Param() params: HandlerParams, @Res() res) {
    return this._destinationsService.findImage(params.id, res);
  }


  @ApiImplicitFile({description: 'File upload', name: 'file'})
  @ApiNotFoundResponse({description: `Destination for the ID doesn"t exist`})
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiUnsupportedMediaTypeResponse({description: "Supports only 'png' an 'jpeg' type"})
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the user in the database', type: String })
  @ApiOkResponse({description: 'Upload completed'})
  @ApiUnauthorizedResponse({description: `Unauthorized and needs admin role`})
  @Post(':id/image')
  @Role('admin')
  @UseGuards(AuthGuard('jwt'))
  uploadFileImage(@Req() req, @Res() reply, @Param() params: HandlerParams) {
    this._destinationsService.uploadFile(req,reply,params.id);
  }

  /**
   * Handler to answer to DELETE /destinations/:id route
   *
   * @param {HandlerParams} params list of route params to take Destination id
   *
   * @returns Observable<void>
   */

  @ApiNoContentResponse({ description: 'The Destination has been successfully deleted' })
  @ApiNotFoundResponse({ description: 'Destination with the given "id" doesn\'t exist in the database' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized and needs admin role`})
  @Delete(':id/image')
  @UseInterceptors(AppInterceptor)
  @Role('admin')
  @UseGuards(AuthGuard('jwt'),RolesGuard)
  deleteImage(@Param() params: HandlerParams): Observable<void> {
    return this._destinationsService.deleteImage(params.id);
  }


  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Traveler in the database', type: String })
  @ApiBadRequestResponse({ description: 'Parameters provided are not good' })
  @ApiNotFoundResponse({ description: "Traveler or destination with the given id doesn't exist" })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(':id/travelers/:idProperty')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  getOneTraveler(@Param() param: Handler2Params): Observable<DestinationUserEntity> {
    return this._destinationsService.findOneUser(param.id, param.idProperty, 'travelers');
  }


  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Contact in the database', type: String })
  @ApiBadRequestResponse({ description: 'Parameters provided are not good' })
  @ApiNotFoundResponse({ description: "Contact or destination with the given id doesn't exist" })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(':id/contacts/:idProperty')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  getOneContact(@Param() param: Handler2Params): Observable<DestinationUserEntity> {
    return this._destinationsService.findOneUser(param.id, param.idProperty, 'contacts');
  }


  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Traveler in the database', type: String })
  @ApiNotFoundResponse({ description: "Traveler or destination with the given id doesn't exist" })
  @ApiBadRequestResponse({ description: 'Parameters provided are not good' })
  @ApiConflictResponse({ description: "Traveler already presents" })
  @ApiOkResponse({ description: 'Traveler is added', type: DestinationUserEntity })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Post(":id/travelers/:idProperty")
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  addTraveler(@Param() params: Handler2Params): Observable<DestinationEntity> {
    return this._destinationsService.findOneAndAddUser(params.id, params.idProperty, 'travelers');
  }

  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiNotFoundResponse({ description: "Destination with the given id doesn't exist" })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiOkResponse({ description: 'Contact is added', type: DestinationUserEntity, isArray: true })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(":id/travelers")
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  findAllTravelers(@Param() params: HandlerParams): Observable<DestinationUserEntity[]> {
    return this._destinationsService.findAllUser(params.id, 'travelers');
  }


  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Contact in the database', type: String })
  @ApiNotFoundResponse({ description: "Contact or destination with the given id doesn't exist" })
  @ApiBadRequestResponse({ description: 'Parameters provided are not good' })
  @ApiConflictResponse({ description: "Contact already presents" })
  @ApiOkResponse({ description: 'Contact is added', type: DestinationUserEntity })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Post(":id/contacts/:idProperty")
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  addContact(@Param() params: Handler2Params): Observable<DestinationEntity> {
    return this._destinationsService.findOneAndAddUser(params.id, params.idProperty, 'contacts');
  }

  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiNotFoundResponse({ description: "Opinion or destination with the given id doesn't exist" })
  @ApiBadRequestResponse({ description: 'Parameters or opinion provided are not good' })
  @ApiConflictResponse({ description: "Opinion already presents" })
  @ApiOkResponse({ description: 'Opinion is added', type: DestinationOpinionEntity })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Post(":id/opinions")
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  addOpinion(@Param() params: HandlerParams,@Body() createOpinionDto:CreateOpinionDto): Observable<DestinationEntity> {
    return this._destinationsService.findOneAndAddOpinion(params.id,createOpinionDto);
  }

  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiNotFoundResponse({ description: "Destination with the given id doesn't exist" })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiOkResponse({ description: 'Contact is added', type: DestinationUserEntity, isArray: true })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(":id/contacts")
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  findAllContact(@Param() params: HandlerParams): Observable<DestinationUserEntity[]> {
    return this._destinationsService.findAllUser(params.id, 'contacts');
  }

  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiNotFoundResponse({ description: "Destination with the given id doesn't exist" })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiOkResponse({ description: 'Opinion is added', type: DestinationOpinionEntity, isArray: true })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Get(":id/opinions")
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  findAllOpinions(@Param() params: HandlerParams): Observable<DestinationOpinionEntity[]> {
    return this._destinationsService.findAllOpinions(params.id);
  }

  /**
   * Handler to answer to DELETE /destinations/:id route
   *
   * @param {HandlerParams} params list of route params to take Destination id
   *
   * @returns Observable<void>
   */

  @ApiNoContentResponse({ description: 'The Destination has been successfully deleted' })
  @ApiNotFoundResponse({ description: 'Destination with the given "id" doesn\'t exist in the database' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized and needs admin role`})
  @Delete(':id')
  @UseInterceptors(AppInterceptor)
  @Role('admin')
  @UseGuards(AuthGuard('jwt'),RolesGuard)
  delete(@Param() params: HandlerParams): Observable<void> {
    return this._destinationsService.delete(params.id);
  }

  /**
   * Handler to answer to DELETE /destinations/:id/contacts/:idProperty route
   *
   * @param {HandlerParams} params list of route params to take Destination id
   *
   * @returns Observable<void>
   */

  @ApiNoContentResponse({ description: 'The Contact has been successfully remove' })
  @ApiNotFoundResponse({ description: 'Destination or Contact with the given "ids" don\'t exist in the database' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Contact in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Delete(':id/contacts/:idProperty')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  deleteContact(@Param() params: Handler2Params): Observable<void> {
    return this._destinationsService.deleteUser(params.id, params.idProperty, 'contacts');
  }

  @ApiNoContentResponse({ description: 'The Contact has been successfully remove' })
  @ApiNotFoundResponse({ description: 'Destination or Contact with the given "ids" don\'t exist in the database' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Contact in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Delete(':id/opinions/:idProperty')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  deleteOpinion(@Param() params: Handler2ParamsNoMongo): Observable<void> {
    return this._destinationsService.deleteOpinion(params.id, params.idProperty);
  }

  /**
   * Handler to answer to DELETE /destinations/:id/contacts/:idProperty route
   *
   * @param {HandlerParams} params list of route params to take Destination id
   *
   * @returns Observable<void>
   */

  @ApiNoContentResponse({ description: 'The Contact has been successfully remove' })
  @ApiNotFoundResponse({ description: 'Destination or Contact with the given "ids" don\'t exist in the database' })
  @ApiUnprocessableEntityResponse({ description: 'The request can\'t be performed in the database' })
  @ApiBadRequestResponse({ description: 'Parameter provided is not good' })
  @ApiImplicitParam({ name: 'id', description: 'Unique identifier of the Destination in the database', type: String })
  @ApiImplicitParam({ name: 'idProperty', description: 'Unique identifier of the Contact in the database', type: String })
  @ApiUnauthorizedResponse({description: `Unauthorized`})
  @Delete(':id/travelers/:idProperty')
  @UseInterceptors(AppInterceptor)
  @UseGuards(AuthGuard('jwt'))
  deleteTraveler(@Param() params: Handler2Params): Observable<void> {
    return this._destinationsService.deleteUser(params.id, params.idProperty, 'travelers');
  }
}
