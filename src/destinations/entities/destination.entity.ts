import { Exclude, Expose, Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';
import { DestinationUserEntity } from './destination.user.entity';
import { DestinationImageEntity } from './destination.image.entity';
import { DestinationOpinionEntity } from './destination.opinion.entity';

@Exclude()
export class DestinationEntity {
  @ApiModelProperty({ description: 'Unique identifier in the database', example: '5763cd4dc378a38ecd387737'})
  @Expose()
  @Type(() => String)
  id: string;

  @ApiModelProperty({ description: 'Country of the destination', example: 'France'})
  @Expose()
  @Type(() => String)
  country: string;

  @ApiModelProperty({ description: `Description of the destination`, example: 'Great place to be'})
  @Expose()
  @Type( () => String)
  description?: string;

  @ApiModelProperty({ description: `Description of the destination`})
  @Expose()
  @Type( () => DestinationImageEntity)
  image?: DestinationImageEntity;

  @ApiModelProperty({ description: 'Region of the destination', example: 'Vosges'})
  @Expose()
  @Type(() => String)
  region: string;

  @ApiModelProperty({ description: 'City of the destination', example: 'Epinal'})
  @Expose()
  @Type(() => String)
  city: string;

  @ApiModelProperty( {description: "User's opinions for the destination", type: [DestinationOpinionEntity]})
  @Expose()
  @Type(() => DestinationOpinionEntity)
  opinions: DestinationOpinionEntity[];

  @ApiModelProperty({ description: 'Contact users for the destination', type: [DestinationUserEntity]})
  @Expose()
  @Type(() => DestinationUserEntity)
  contacts: DestinationUserEntity[];

  @ApiModelProperty({ description: 'Traveler users for the destination', type: [DestinationUserEntity]})
  @Expose()
  @Type(() => DestinationUserEntity)
  travelers: DestinationUserEntity[];

  /**
   * Class constructor
   *
   * @param partial data to insert in object instance
   */
  constructor(partial: Partial<DestinationEntity>) {
    Object.assign(this, partial);
  }

}
