import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class DestinationOpinionEntity {
  @ApiModelProperty({ description: 'ID user', example: '5763cd4dc378a38ecd387737'})
  @Expose()
  @Type(() => String)
  idUser: string;

  @ApiModelProperty({ description: 'Id opinion', example: '5763cd4dc378a38ecd387737'})
  @Expose()
  @Type(() => String)
  idOp: string;

  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @Expose()
  @Type(() => String)
  firstname: string;

  @ApiModelProperty({ description: 'Lastname', example: 'Saunier'})
  @Expose()
  @Type(() => String)
  lastname: string;

  @ApiModelProperty({ description: 'Firstname', example: "I'm a comment"})
  @Expose()
  @Type(() => String)
  comment: string;

  /**
   * Class constructor
   *
   * @param partial data to insert in object instance
   */
  constructor(partial: Partial<DestinationOpinionEntity>) {
    Object.assign(this, partial);
  }
}
