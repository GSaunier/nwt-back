import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';


@Exclude()
export class DestinationUserEntity {
  @ApiModelProperty({ description: 'Unique identifier in the database', example: '5763cd4dc378a38ecd387737'})
  @Expose()
  @Type(() => String)
  id: string;

  @ApiModelProperty({ description: 'Firsname', example:'Guillaume'})
  @Expose()
  @Type(() => String)
  firstname: string;

  @ApiModelProperty({ description: 'Lastname', example:'Saunier'})
  @Expose()
  @Type(() => String)
  lastname: string;

  @ApiModelProperty( { description: 'Email', example: "example@test.com"})
  @Expose()
  @Type(() => String)
  email: string;

  /**
   * Class constructor
   *
   * @param partial data to insert in object instance
   */
  constructor(partial: Partial<DestinationUserEntity>) {
    Object.assign(this, partial);
  }
}
