import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';


@Exclude()
export class DestinationImageEntity {
  @ApiModelProperty({ description: 'Type', example:'image/png'})
  @Expose()
  @Type(() => String)
  type: string;

  @ApiModelProperty({ description: 'Filename', example:'image.png'})
  @Expose()
  @Type(() => String)
  filename: string;

  /**
   * Class constructor
   *
   * @param partial data to insert in object instance
   */
  constructor(partial: Partial<DestinationImageEntity>) {
    Object.assign(this, partial);
  }
}
