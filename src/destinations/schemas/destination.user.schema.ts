import * as mongoose from 'mongoose';

export const DestinationUserSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true,
    minlength: 4,
    trim: true,
  },
  lastname: {
    type: String,
    required: true,
    minlength: 4,
    trim: true,
  },
  email: {
    type: String,
    match: /[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/,
    required: true,
    trim: true,
  },
}, {
  toJSON: { virtuals: true },
  versionKey: false,
});
