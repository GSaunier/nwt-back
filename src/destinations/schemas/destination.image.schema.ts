import * as mongoose from 'mongoose';

export const DestinationImageSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
    match: /^[-\w.]+\/[-\w.]+$/,
    trim: true,
  },
  filename: {
    type: String,
    required: true,
    trim: true,
  }
}, {
  toJSON: { virtuals: true },
  versionKey: false,
});
