import * as mongoose from 'mongoose';
import { DestinationImageSchema } from './destination.image.schema';
import { DestinationUserSchema } from './destination.user.schema';

export const DestinationSchema = new mongoose.Schema({
  country: {
    type: String,
    required: true,
    minlength: 3,
    trim: true,
  },
  region: {
    type: String,
    required: true,
    minlength: 3,
    trim: true,
  },
  city: {
    type: String,
    required: true,
    minlength: 3,
    trim: true,
  },
  description: {
    type: String,
  },
  image: {
    type: DestinationImageSchema,
  },
  opinions: [{

  idUser: {
    type: mongoose.Types.ObjectId,
      required: true,
  },
  idOp: {
    type: String,
    required: true,
  },
  firstname: {
    type: String,
      required: true,
      minlength: 4,
      trim: true,
  },
  lastname: {
    type: String,
      required: true,
      minlength: 4,
      trim: true,
  },
  comment: {
    type: String,
      required: true
  }
  }],
  contacts: {
    type: [DestinationUserSchema],
  },
  travelers: {
    type: [DestinationUserSchema],
  }

}, {
  toJSON: { virtuals: true },
  versionKey: false,

}).index({country: 1, region: 1, city: 1},{unique: true});
