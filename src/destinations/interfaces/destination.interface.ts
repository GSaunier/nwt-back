import { DestinationImage } from './destination.image.interface';
import { DestinationUser } from './destination.user.interface';
import { Document } from 'mongoose';
import { DestinationOpinion } from './destination.opinion.interface';

export interface Destination extends Document{
  id: string;
  country: string;
  region: string;
  description?: string;
  image?: DestinationImage;
  opinions: DestinationOpinion[];
  city: string;
  contacts: DestinationUser[];
  travelers: DestinationUser[];
}
