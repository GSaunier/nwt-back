export interface DestinationUser {
  id: string;
  firstname: string;
  lastname: string;
  email:string;
}
