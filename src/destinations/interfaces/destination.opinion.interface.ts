import { Document } from 'mongoose';

export class DestinationOpinion extends Document{
  idUser: string;
  idOp: string;
  firstname: string;
  lastname: string;
  comment: string;
}

