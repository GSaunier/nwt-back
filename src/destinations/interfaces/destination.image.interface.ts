import { Document } from 'mongoose';

export interface DestinationImage extends Document {
  type: string;
  filename: string;
}
