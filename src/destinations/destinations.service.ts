import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnprocessableEntityException, UnsupportedMediaTypeException,
} from '@nestjs/common';
import { Observable, of, throwError } from 'rxjs';
import { catchError, flatMap, map, tap } from 'rxjs/operators';
import { DestinationEntity } from './entities/destination.entity';
import { UpdateDestinationDto } from './dto/update-destination.dto';
import { DestinationUserEntity } from './entities/destination.user.entity';
import { DocumentsService } from '../documents/documents.service';
import { DestinationDao } from './dao/destination.dao';
import * as fs from 'fs';
import { CreateDestinationDto } from './dto/create-destination.dto';
import { CreateOpinionDto } from './dto/create-opinion.dto';
import { DestinationOpinionEntity } from './entities/destination.opinion.entity';

@Injectable()
export class DestinationsService {
  //desitnations static
  //private _destinations: Destination[];
  private readonly _pathDestinations: string;

  constructor(private readonly _documentsService: DocumentsService, private readonly _destinationDao: DestinationDao) {
    //this._destinations = DEST;
    this._pathDestinations = "assets/destinations";
  }

  /**
   * Get all destinations
   * @returns {Observable<DestinationEntity>} list of all destinations
   */
  findAll(): Observable<DestinationEntity[]> {
    return this._destinationDao.find().pipe(
      catchError(e => throwError(new UnprocessableEntityException(e.message))),
      map(_ => (!!_ && !!_.length) ? _.map(__ => new DestinationEntity(__)) : undefined),
    );
  }

  findAllUser(id:string, type: string): Observable<DestinationUserEntity[]> {
    return this._destinationDao.findAllUser(id,type).pipe(
      catchError(e => throwError(new UnprocessableEntityException(e.message))),
      map(_ => (!!_ && !!_.length) ? _.map(__ => new DestinationUserEntity(__)) : undefined),
    );
  }

  findAllOpinions(id:string): Observable<DestinationOpinionEntity[]> {
    return this._destinationDao.findAllOpinions(id).pipe(
      catchError(e => throwError(new UnprocessableEntityException(e.message))),
      map(_ => (!!_ && !!_.length) ? _.map(__ => new DestinationOpinionEntity(__)) : undefined),
    );
  }


  /**
   * Get a destination matching with ID
   * @param id of the destination
   *
   * @returns {Observable<DestinationEntity>} destination
   */
  findOne(id: string): Observable<DestinationEntity> {
    return this._destinationDao.findById(id).pipe(
      catchError(e => throwError(new UnprocessableEntityException(e.message))),
      flatMap(_ => !!_ ?
        of(new DestinationEntity(_)) :
        throwError(new NotFoundException(`No Destination for id: ${id}`))
      ),
    );
  }

  /**
   * Find a destination update with the payload
   */
  findOneAndUpdate(id: string, updateDestinationDto: UpdateDestinationDto): Observable<DestinationEntity> {
    return this._destinationDao.findOneAndUpdate(id,updateDestinationDto).pipe(
      catchError(e =>
        e.code === 11000 ?
          throwError(
            new ConflictException(e.message),
          ) :
          throwError(new UnprocessableEntityException(e.message)),
      ),
      flatMap(_ =>
        !!_ ?
          of(new DestinationEntity((_))) :
          throwError(new NotFoundException(`User with id '${id}' not found`)),
      ),
    );
  }

  /**
   * Find a destination and add the user in type of list if exists and doesn't present
   */
  findOneAndAddUser(id: string, idProperty: string, type: string): Observable<DestinationEntity> {
    return this._destinationDao.addUser(id,idProperty,type).pipe(
      catchError(e =>
        e.code ===11000 ?
          throwError(
            new ConflictException(e.message)
          ) :
          throwError(new UnprocessableEntityException(e.message)),
      ),
      flatMap(_ =>
        !!_ ?
          of(new DestinationEntity((_))) :
          throwError(new NotFoundException(`Destination with id '${id}' not found`)),
      ),
    )
  }

  /**
   * Find a destination and add the user in type of list if exists and doesn't present
   */
  findOneAndAddOpinion(id: string,createOpinionDto:CreateOpinionDto): Observable<DestinationEntity> {
    return this._destinationDao.addOpinion(id,createOpinionDto).pipe(
      catchError(e =>
        e.code ===11000 ?
          throwError(
            new ConflictException(e.message)
          ) :
          throwError(new UnprocessableEntityException(e.message)),
      ),
      flatMap(_ =>
        !!_ ?
          of(new DestinationEntity((_))) :
          throwError(new NotFoundException(`Destination with id '${id}' not found`)),
      ),
    )
  }



  /**
   * Find one contact matching with id in the specific Destination
   * @param idDestination
   * @param idUser
   * @param type
   */
  findOneUser(idDestination:string, idUser: string, type: string): Observable<DestinationUserEntity> {
    return this._destinationDao.findOneUser(idDestination, idUser, type).pipe(
      flatMap(_ => !!_ ? of(new DestinationUserEntity(_)) : throwError(new NotFoundException(`Contact '${idUser}'not found in list of ${type} in Destination '${idDestination}'`)))
    )
  }


  deleteUser(idDest: string, idUser: string, type: string): Observable<void> {
    return this._destinationDao.removeUser(idDest,idUser,type).pipe(
      flatMap(_ => !!_ ?
        of(undefined) :
        throwError(new NotFoundException(`The Destination with ID '${idDest}' doesn't exist`))
      ))
  }

  deleteOpinion(idDest: string, idOp: string): Observable<void> {
    return this._destinationDao.removeOpinion(idDest,idOp).pipe(
      flatMap(_ => !!_ ?
        of(undefined) :
        throwError(new NotFoundException(`The Destination with ID '${idDest}' doesn't exist`))
      ))
  }

  findImage(id: string, res) {
    return this._destinationDao.findByIdImage(id).pipe(
      flatMap(_ => !!_ ?
        of(_) :
        throwError(new NotFoundException('No File found'))),
      tap(_ => res.type(_.type).send(fs.createReadStream(this._pathDestinations+'/'+ id + '/' + _.filename) )),
    )
  }


  uploadFile(req, reply, id: string) {
    let error;
    req.multipart((field, file, filename, encoding, mimetype) => {
      if(mimetype === "image/png" || mimetype === "image/jpeg") {
        this.findOne(id).subscribe(_ => _, error => reply.code(error.message.statusCode).send(error.message.message));
      }else {
        error = new UnsupportedMediaTypeException("Supports only jpeg and png");
        reply.code(error.message.statusCode).send(error.message.message);
      }
      if(!reply.sent) {
        const folder: string = this._pathDestinations + '/' + id;
        this._documentsService.uploadFile(folder, filename, file);
        this._destinationDao.findByIdUpdateImage(id, { image: { type: mimetype, filename: filename }}).subscribe(_ => _, e => error = e)
      }
    }, () => {
      if(!error)
        reply.code(200).send('uploaded');
      else
        reply.code(error.message.statusCode).send(error.message.message)
    });
  }

  deleteImage(id: string) {
    return  this._destinationDao.findByIdRemoveImage(id).pipe(
      catchError(e =>
        e.code ===11000 ?
          throwError(
            new ConflictException(e.message),
          ) :
          throwError(new UnprocessableEntityException(e.message)),
      ),
      flatMap(_ =>
        !!_ ?
          of(undefined) :
          throwError(new NotFoundException(`User with id '${id}' not found`)),
      ),
      tap(_ => this._documentsService.deleteFile(this._pathDestinations + '/' + id)),
    )
  }

  /*

  /!**
   * Find a destination update with the payload
   *!/
  findOneAndUpdateImage(id: string, updateDestinationImageDto: UpdateDestinationImageDto): Observable<DestinationEntity> {
    return from(this._destinations).pipe(
      findIndex(_ => _.id === id),
      flatMap(_ => _ > -1 ? of(_) : throwError(new NotFoundException(`The destination with ID '${id}' doesn't exist`))),
      tap(_ => Object.assign(this._destinations[_],updateDestinationImageDto)),
      map(_ => new DestinationEntity(this._destinations[_]))
    )
  }
*/
  /**
   * Check if user already exists and add it in users list
   *
   * @param {CreateDestinationDto} createDestinationDto to create
   *
   * @returns {Observable<DestinationEntity>}
   */
  create(createDestinationDto: CreateDestinationDto): Observable<DestinationEntity> {
    return this._destinationDao.create(createDestinationDto).pipe(
      catchError(e => e.code ===11000 ?
        throwError(
          new ConflictException(e.message),
        ) :
        throwError(new UnprocessableEntityException())
      ),
      map(_ => new DestinationEntity(_))
    );
  }

  delete(id:string): Observable<void> {
    return this._destinationDao.findOneAndRemove(id).pipe(
      catchError(e => throwError(new NotFoundException(e.message))),
      flatMap(_ =>
        !!_ ?
          of(_) :
          throwError(new NotFoundException(`Destination with id '${id}' not found`)),
      ),
      flatMap(_ => !!_.image ? of(this._documentsService.deleteFile(this._pathDestinations+'/'+id)) :
        of(undefined)
      )
    )
  }
}
