import { ApiModelProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class ImageDestinationDto {
  @ApiModelProperty({ description: 'Type', example: 'image/png' })
  @IsString()
  type: string;

  @ApiModelProperty({ description: 'Filename', example: 'image.png' })
  @IsString()
  filename: string;
}
