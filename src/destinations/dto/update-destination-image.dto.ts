import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ImageDestinationDto } from './image.destination.dto';
import { Optional } from '@nestjs/common';

export class UpdateDestinationImageDto {
  @ApiModelProperty({description: 'Image name', example: 'image.png'})
  @ValidateNested()
  @Type(() => ImageDestinationDto)
  @IsOptional()
  image?: ImageDestinationDto
}
