import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MinLength } from 'class-validator';
import { Optional } from '@nestjs/common';

export class UpdateDestinationDto {
  @ApiModelProperty({ description: 'Country', example: 'France'})
  @IsString()
  @MinLength(3)
  @IsOptional()
  country?: string;

  @ApiModelProperty({ description: 'Region', example: 'Vosges'})
  @IsString()
  @MinLength(3)
  @IsOptional()
  region?: string;

  @ApiModelProperty({ description: 'City', example: 'Epinal'})
  @IsString()
  @MinLength(3)
  @IsOptional()
  city?: string;

  @ApiModelProperty({ description: 'Description', example: 'Great place to be'})
  @IsString()
  @IsOptional()
  description?: string;
}
