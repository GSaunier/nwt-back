import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsMongoId, IsString, MinLength } from 'class-validator';

export class CreateOpinionDto {
  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @IsMongoId()
  @Type(() => String)
  idUser: string;

  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @IsString()
  @MinLength(2)
  firstname: string;

  @ApiModelProperty({ description: 'Lastname', example: 'Saunier'})
  @IsString()
  @MinLength(2)
  lastname: string;

  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @IsString()
  comment: string;
}
