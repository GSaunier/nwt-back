import { ApiModelProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty, IsOptional, IsString, Matches, MinLength } from 'class-validator';
import { Optional } from '@nestjs/common';

export class UserDestinationDto {
  @ApiModelProperty({description: "MongoID", example: "5763cd4dc378a38ecd387737"})
  @IsMongoId()
  @IsNotEmpty()
  id: string;

  @ApiModelProperty({ description: 'Firstname', example: 'Guillaume'})
  @IsString()
  @MinLength(2)
  @IsOptional()
  firstname?: string;

  @ApiModelProperty({ description: 'Lastname', example: 'Saunier'})
  @IsString()
  @MinLength(2)
  @IsOptional()
  lastname?: string;

  @ApiModelProperty({ description: 'Email', example: 'example@test.com'})
  @IsString()
  @IsOptional()
  @Matches(/[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}/)
  email?: string;
}
