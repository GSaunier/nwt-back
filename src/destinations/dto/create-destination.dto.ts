import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';
import { Optional } from '@nestjs/common';

export class CreateDestinationDto {
  @ApiModelProperty({ description: 'Country', example: 'France'})
  @IsString()
  @MinLength(3)
  country: string;

  @ApiModelProperty({ description: 'Region', example: 'Vosges'})
  @IsString()
  @MinLength(3)
  region: string;

  @ApiModelProperty({ description: 'City', example: 'Epinal'})
  @IsString()
  @MinLength(3)
  city: string;

  @ApiModelProperty({ description: 'Description', example: 'Great place to be'})
  @IsString()
  @Optional()
  description?: string;
}
