import { ConflictException, Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, MongooseDocument } from 'mongoose';
import { Destination } from '../interfaces/destination.interface';
import { from, Observable, of, throwError } from 'rxjs';
import { catchError, find, flatMap, map } from 'rxjs/operators';
import { UpdateDestinationDto } from '../dto/update-destination.dto';
import { DestinationUser } from '../interfaces/destination.user.interface';
import { UserDao } from '../../users/dao/user.dao';
import { UserDestinationDto } from '../dto/user-destination.dto';
import { DestinationImage } from '../interfaces/destination.image.interface';
import { CreateDestinationDto } from '../dto/create-destination.dto';
import { UpdateDestinationImageDto } from '../dto/update-destination-image.dto';
import { DestinationOpinion } from '../interfaces/destination.opinion.interface';
import { CreateOpinionDto } from '../dto/create-opinion.dto';

@Injectable()
export class DestinationDao {
  constructor(@InjectModel('Destination') private readonly _destinationModel: Model<Destination>, private readonly _userDao: UserDao) {}

  create(createDestinationDto:CreateDestinationDto): Observable<Destination> {
    return from(this._destinationModel.create(createDestinationDto)).pipe(
      map((doc: MongooseDocument) => doc.toJSON())
    );
  }

  /**
   * Get all Destination in the database
   *
   * @return {Observable<Destination[]>} array of all user in the database
   */
  find(): Observable<Destination[]> {
    return this._toJSONArray<Destination>(from(this._destinationModel.find({})))
  }

  findById(id: string) {
    return this._toJSON<Destination>(from(this._destinationModel.findById(id)))
  }

  findOneAndUpdate(id: string, updateDestinationDto: UpdateDestinationDto): Observable<Destination> {
    return this._toJSON<Destination>(from(this._destinationModel.findByIdAndUpdate(id,updateDestinationDto, {new: true})))
  }


  findOneUser(idDestination:string, idUser: string, type: string): Observable<DestinationUser> {
    return  this._toJSON<DestinationUser>(from(this._destinationModel.findById(idDestination))).pipe(
      flatMap(_ => from(_[type]).pipe(
        find((_: DestinationUser) => _.id === idUser),
        flatMap(_ => !!_ ? of(_) : of(undefined)))
      ));
  }

  findOneOpinion(idDestination:string, idOp: string): Observable<DestinationOpinion> {
    return  this._toJSON<Destination>(from(this._destinationModel.findById(idDestination))).pipe(
      flatMap((_ : Destination) => from(_.opinions).pipe(
        find(_ => _.idOp === idOp),
        flatMap(_ => !!_ ? of(_) : of(undefined)))
      ));
  }

  findAllOpinions(id: string): Observable<DestinationOpinion[]> {
    return  this._toJSON<DestinationUser>(from(this._destinationModel.findById(id))).pipe(
      flatMap(_ => of(_["opinions"])))
  }

  addOpinion(id: string, createOpinionDto :CreateOpinionDto): Observable<Destination> {
    return this._toJSON(this._addIdOpinion(createOpinionDto).pipe(
      flatMap(opinion => from(this._destinationModel.findById(id)).pipe(
        flatMap(dest => !!dest ? of(dest) : throwError(new NotFoundException("Destination not exist"))),
        flatMap((_: Destination) => this.checkOpinion(_, opinion.idOp, id, opinion.idUser)),
        flatMap(_ => this._destinationModel.findByIdAndUpdate(id,
          {$push: {
              "opinions": opinion
            }}, {new : true})),
        ))
      ))
      ;

  }

  private _addIdOpinion(createOpinionDto: CreateOpinionDto): Observable<DestinationOpinion> {
    return of(createOpinionDto)
      .pipe(
        map(_ =>
          Object.assign(_, {
            idOp: DestinationDao.createId(),
          }) as DestinationOpinion,
        )
      );
  }

  checkOpinion(dest: Destination, idOp: string, idDest: string, idUs: string): Observable<Destination> {
    return this.findOneOpinion(idDest,idOp).pipe(
      flatMap(_ => !_ ? of(_) : throwError(new ConflictException(`Opinion '${idOp}' already presents in Destination '${idDest}'`))),
      flatMap(_ => this._userDao.findOneById(idUs)),
      flatMap(_ => !!_ ? of(dest) : throwError(new ConflictException(`User '${idUs}' doesn't exist`))))
  }

  removeOpinion(id: string,idOp: string): Observable<Destination> {
    return this.findOneOpinion(id, idOp).pipe(
      flatMap(_ => !!_ ? this._toJSON<Destination>(from(this._destinationModel.findByIdAndUpdate(id, { $pull: { opinions: { idOp: idOp } } }, { new: true }))) :
        throwError(new NotFoundException(`Opinion '${idOp}' doesn't present in the list of Opinions in Destination '${id}'`))
      )
    );
  }

  findAllUser(id: string, type: string): Observable<DestinationUser[]> {
    return  this._toJSON<DestinationUser>(from(this._destinationModel.findById(id))).pipe(
      flatMap(_ => of(_[type])))
  }

  addUser(id: string, idProperty: string, type: string): Observable<Destination> {
    return this._toJSON(from(this._destinationModel.findById(id)).pipe(
      flatMap(dest => !!dest ? of(dest) : throwError(new NotFoundException("Destination not exist"))),
      flatMap(() => this._userDao.findOneById(idProperty).pipe(
        catchError(e => throwError(new UnprocessableEntityException(e.message))),
        flatMap(_ => !!_ ?
          of(_) :
          throwError(new NotFoundException(`No user for id: ${idProperty}`))
        ),
        flatMap(user => this.findOneUser(id,idProperty,type).pipe(
          flatMap(_ => !!_ ?
            throwError(new ConflictException(`User already presents in the list of ${type}`)) :
            of(user)
          ),
          flatMap(_ => this._destinationModel.findByIdAndUpdate(id,
            {$push: {
                [type]: user as UserDestinationDto
              }}, {new : true}))
          ),
        )),
      ))
    );
  }

  findOneAndRemove(id: string): Observable<Destination> {
    return this._toJSON(from(this._destinationModel.findByIdAndRemove(id)))
  }

  removeUser(id: string,idUser: string, type: string): Observable<Destination> {
    return this.findOneUser(id, idUser, type).pipe(
      flatMap(_ => !!_ ? this._toJSON<Destination>(from(this._destinationModel.findByIdAndUpdate(id, { $pull: { [type]: { _id: idUser } } }, { new: true }))) :
        throwError(new NotFoundException(`User '${idUser}' doesn't present in the list of ${type} in Destination '${id}'`))
      )
    );
  }

  findByIdImage(id: string): Observable<DestinationImage> {
    return this._toJSON(from(this._destinationModel.findById(id, {image: 1})).pipe(
      flatMap(_  => !!_ ?  of(_.image) : throwError(new NotFoundException(`No Destination '${id}`)))
    ));
  }

  findByIdUpdateImage(id: string, updateDestinationImageDto:UpdateDestinationImageDto): Observable<Destination> {
    return this._toJSON(from(this._destinationModel.findByIdAndUpdate(id,updateDestinationImageDto,{new: true})))
  }

  findByIdRemoveImage(id:string): Observable<Destination> {
    return this._toJSON(from(this._destinationModel.findByIdAndUpdate(id,{$unset: {image:1}}, {new: true})))
  }

  /**
   * Transform array of document in the corresponding class
   *
   * @param {Observable<MongooseDocument[]>}obs array of MongooseDocument
   * @private
   *
   * @return {Observable<T[]>} array of class transformed
   */
  private _toJSONArray<T>(obs: Observable<MongooseDocument[]>): Observable<T[]> {
    return obs.pipe(
      map((docs: MongooseDocument[]) => (!!docs && docs.length > 0) ? docs.map(_ => _.toJSON()) : undefined)
    )
  }

  /**
   * Transform a MongooseDocument in the corresponding class
   *
   * @param {Observable<MongooseDocument>} obs of the document
   * @private
   *
   * @return {Observable<T>} class transformed
   */
  private _toJSON<T>(obs: Observable<MongooseDocument>): Observable<T> {
    return obs.pipe(
      map((doc: MongooseDocument) => !!doc ? doc.toJSON() : undefined)
    )
  }

  static createId(): string {
    return `${new Date().getTime()}`;
  }
}
