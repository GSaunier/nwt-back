import { Module } from '@nestjs/common';
import { DestinationsController } from './destinations.controller';
import { DestinationsService } from './destinations.service';
import { DocumentsService } from '../documents/documents.service';
import { UsersModule } from '../users/users.module';
import { DestinationDao } from './dao/destination.dao';
import { MongooseModule } from '@nestjs/mongoose';
import { DestinationSchema } from './schemas/destination.schema';
import { DestinationImageSchema } from './schemas/destination.image.schema';
import { DestinationUserSchema } from './schemas/destination.user.schema';
import { UserDao } from '../users/dao/user.dao';
import { UserSchema } from '../users/schemas/user.schema';

@Module({
  imports: [UsersModule,MongooseModule.forFeature([{name: 'User', schema: UserSchema},{name: 'Destination', schema: DestinationSchema}, {name: 'DestinationImage', schema: DestinationImageSchema}, {name: "DestinationUser",schema: DestinationUserSchema}])],
  controllers: [DestinationsController],
  providers: [DestinationsService, DocumentsService, DestinationDao, UserDao]
})
export class DestinationsModule {}
